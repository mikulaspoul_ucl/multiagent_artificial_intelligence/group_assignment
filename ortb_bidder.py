import gc

import lightgbm
import pandas as pd

from  tqdm import tqdm

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa

from linear_bidder_gbm import LinearBidder, BUDGET, test_path, validation_path


# lbd_range = np.arange(0.00000001, 0.00000001 * 100, 0.00000001)
# c_range = range(1, 40, 1)


def load_model():
    return lightgbm.Booster(model_file="models/proper_types_full.model")



class ORTBBidder(LinearBidder):

    version = 1

    def get_bids(self, x, pctr, lbd, c):
        pctr2 = pctr.copy()
        pctr2[pctr2 < 0.0005] = 0

        bids = np.sqrt(np.add(np.multiply(np.divide(c, lbd), pctr2), np.power(c, 2))) - c

        return bids

    def get_won_subset(self, x, y, bid, budget):
        pctr, lbd, c = bid

        bids = self.get_bids(x, pctr, lbd, c)

        dataset = y[bids >= y["payprice"]]

        spend = dataset['payprice'].cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend.iloc[-1]
        return budget if max_spend > budget else max_spend, dataset

    def bid_on_test(self, bid):
        model, lbd, c = bid

        test_x, test_ids = self.load_dataset(test_path, test=True)

        print("Loaded test")

        bids = self.get_bids(test_x, model.predict(test_x), lbd, c)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df

    def bid_on_validation(self, bid):
        model, lbd, c = bid

        test_x, test_ids = self.load_dataset(validation_path, test=True)

        print("Loaded validation")

        bids = self.get_bids(test_x, model.predict(test_x), lbd, c)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df

    def hist(self, model):
        test_x, test_ids = self.load_dataset(test_path, test=True)

        pctr = model.predict(test_x)

        fix, ax = plt.subplots(figsize=(10, 6))
        ax.hist(pctr, bins=200, log=True)
        plt.savefig(f"figures/pctr_prediction.pdf")

        pctr[pctr < 0.0006] = 0

        print(np.count_nonzero(pctr))

        fix, ax = plt.subplots(figsize=(10, 6))
        ax.hist(pctr, bins=200, log=True)
        plt.savefig(f"figures/pctr_prediction_reduced.pdf")



class ORTB2Bidder(ORTBBidder):

    version = 2

    def get_bids(self, x, pctr, lbd, c):
        c_lbd = c * lbd

        pctr2 = pctr.copy()
        pctr2[pctr2 < 0.0005] = 0

        pctr_plus_root = pctr2 + np.sqrt(c**2 * lbd **2 + pctr2 **2)

        bids = c * (np.power(pctr_plus_root / c_lbd, 1/3) - np.power(c_lbd / pctr_plus_root, 1/3))

        return bids


def run_ortb_bidder(bidder: ORTBBidder):
    clicks_train = np.zeros((len(c_range), len(lbd_range)))
    spent = np.zeros((len(c_range), len(lbd_range)))

    model = load_model()

    pctr = model.predict(bidder.X_validation)

    for k, c in tqdm(enumerate(c_range), total=len(c_range)):
        for j, lbd in tqdm(enumerate(lbd_range), total=len(lbd_range), leave=False):
            result, train_data = bidder.evaluate_val_bid_strategy((pctr, lbd, c), BUDGET)

            clicks_train[k][j] = result["clicks"]
            spent[k][j] = result["spent"]

    for i in (-np.array(clicks_train.flat)).argsort()[:25]:
        x, y = np.unravel_index(i, clicks_train.shape)

        best_lambda = list(lbd_range)[y]
        best_c = list(c_range)[x]
        best_clicks = clicks_train[x][y]

        print(best_lambda, best_c, best_clicks, spent[x][y])

    plot(clicks_train, f"figures/ortb{bidder.version}_validation.pdf", azim=170 if bidder.version == 2 else -80)


def plot(mean_clicks, filename, azim):
    fig = plt.figure(figsize=(8, 4.8))
    ax = fig.add_subplot(111, projection='3d')

    x_axis, y_axis = np.meshgrid(lbd_range, c_range)

    ax.plot_wireframe(x_axis, y_axis, mean_clicks,
                      rstride=10, cstride=10, label="Clicks on validation set")

    fig.tight_layout()

    ax.set_xlabel("$\lambda$")
    ax.set_ylabel("C")
    ax.set_zlabel("Clicks")

    ax.set_xticks(xticks_range)

    ax.view_init(azim=azim, elev=20)

    ax.legend()
    fig.tight_layout()

    plt.savefig(filename, tight_layout=True)

    # plt.show()


# {'impressions': 146388, 'clicks': 111, 'CTR': 0.0007582588736781703, 'spent': 6250.0, 'CPM': 42.69475640079788, 'CPC': 56.306306306306304}

if __name__ == "__main__":
    # lbd_range = np.arange(0.00000052, 0.00000452, 0.00000001)
    # c_range = range(10, 200, 5)
    # xticks_range = np.arange(0.0000005, 0.00000451, 0.000001)

    model = load_model()

    bidder = ORTBBidder(load_train=False, load_validation=False)
    # df = bidder.bid_on_validation((model, 3e-06, 290))
    # df.to_csv(f"results/val_bid_price_ortb1_reduced2_300_290.csv", index=None)

    l = 3e-06
    c = 290

    df = bidder.bid_on_test((model, l, c))
    df.to_csv(f"results/test_bidding_price_types_ortb{bidder.version}_reduced2_{l}__{c}.csv", index=None)
    plt.hist(df["bidprice"].values, bins=100, log=True)
    plt.savefig(f"figures/test_bids_ortb{bidder.version}_reduced2_{l}__{c}.pdf")
    plt.clf()

    # run_ortb_bidder(bidder)

    # del bidder
    # gc.collect()

    # lbd_range = np.arange(0.00000001, 0.00000001 * 100, 0.00000001)
    # c_range = range(1, 40, 1)
    # xticks_range = np.arange(0.00000001, 0.00000001 * 100, 0.00000001 * 20)

    # bidder = ORTB2Bidder(load_train=False, load_validation=False)
    #
    # df = bidder.bid_on_validation((model, 4.7e-07, 128))
    # df.to_csv(f"results/val_bid_price_ortb2_reduced2_4.7e-07_128.csv", index=None)

    # run_ortb_bidder(bidder)

    # bidder = ORTB2Bidder(load_train=False, load_validation=False)
    #
    # run_ortb_bidder(bidder)
    #
    # best_lambda = e
    # best_c = 195
    # clicks_on_train = 134
    # clicks_on_test = 150
    #
    # #
    # model = load_model()
    #
    # bidder.hist(model)
    #
    # for l in [4.7e-07]:
    #     for c in [128, 144]:
    #         df = bidder.bid_on_test((model, l, c))
    #         df.to_csv(f"results/test_bidding_price_types_ortb{bidder.version}_reduced2_{l}__{c}.csv", index=None)
    #
    #         plt.hist(df["bidprice"].values, bins=100, log=True)
    #         plt.xlim(right=4000)
    #         plt.title(f"l={l}   c={c}")
    #         plt.savefig(f"figures/test_bids_ortb{bidder.version}_reduced2_{l}_{c}.pdf")
    #         plt.clf()

    # for c in 64+16, 64 + 32, 64 + 48, 64 + 64:
    #     df = bidder.bid_on_test((load_model(), 9.7e-07, c))
    #
    #     df.to_csv(f"results/test_bidding_price_types_ortb{bidder.version}_9.7e-07__{c}.csv", index=None)
    #
    #     plt.hist(df["bidprice"].values, bins=100, log=True)
    #     plt.savefig(f"figures/test_bids_ortb{bidder.version}_{c}.pdf")
    #     plt.clf()

    # bidder = ORTBBidder(load_validation=False, load_train=False)
    # model = load_model()
    #
    # l = 0.00000400
    # c = 195
    #
    # for l in [0.00000398]:
    #
    #     df = bidder.bid_on_test((model, l, c))
    #     df.to_csv(f"results/test_bidding_price_types_ortb{bidder.version}_{l}__{c}.csv", index=None)
    #
    #     plt.hist(df["bidprice"].values, bins=100, log=True)
    #     plt.title(f"l={l}   c={c}")
    #     plt.savefig(f"figures/test_bids_ortb{bidder.version}_{l}_{c}.pdf")
    #     plt.clf()

