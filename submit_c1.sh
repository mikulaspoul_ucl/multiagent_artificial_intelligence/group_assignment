#!/bin/bash

TOKEN=`cat secret_token`

curl http://deepmining.cs.ucl.ac.uk/api/upload/wining_criteria_1/$TOKEN -X Post -F 'file=@results/testing_bidding_price_linear_regression.csv'


# attempt 1 (with random spread 10)
#{
#  "best result": {
#    "clicks": 141,
#    "cost": 6249.999000003198,
#    "cpc": 44.32623404257587,
#    "ctr": 0.000948543212534225,
#    "impressions": 148649
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 7,
#  "result": {
#    "clicks": 141,
#    "cost": 6249.999000003198,
#    "cpc": 44.32623404257587,
#    "ctr": 0.000948543212534225,
#    "impressions": 148649
#  },
#  "today tried times": 1
#}

# attempt 2 (just linear)
#{
#  "best result": {
#    "clicks": 144,
#    "cost": 6249.9990000033795,
#    "cpc": 43.402770833356804,
#    "ctr": 0.0009533708943810703,
#    "impressions": 151043
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 7,
#  "result": {
#    "clicks": 143,
#    "cost": 6249.998000003278,
#    "cpc": 43.706279720302646,
#    "ctr": 0.0009465622579812408,
#    "impressions": 151073
#  },
#  "today tried times": 3
#}

# attempt3 (with random spread 20)
#{
#  "best result": {
#    "clicks": 144,
#    "cost": 6249.9990000033795,
#    "cpc": 43.402770833356804,
#    "ctr": 0.0009533708943810703,
#    "impressions": 151043
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 7,
#  "result": {
#    "clicks": 139,
#    "cost": 6249.99900000293,
#    "cpc": 44.964021582754896,
#    "ctr": 0.000979894537969151,
#    "impressions": 141852
#  },
#  "today tried times": 4
#}

# testing_bidding_price_ortb_252__130.csv
#
#{
#  "best result": {
#    "clicks": 144,
#    "cost": 6249.9990000033795,
#    "cpc": 43.402770833356804,
#    "ctr": 0.0009533708943810703,
#    "impressions": 151043
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 8,
#  "result": {
#    "clicks": 128,
#    "cost": 6249.999000002778,
#    "cpc": 48.828117187521705,
#    "ctr": 0.0008891976380687739,
#    "impressions": 143950
#  },
#  "today tried times": 1
#}

# testing_bidding_price_ortb_305__180.csv
#{
#  "best result": {
#    "clicks": 146,
#    "cost": 6165.352000003314,
#    "cpc": 42.228438356187084,
#    "ctr": 0.0009607981205209369,
#    "impressions": 151957
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 12,
#  "result": {
#    "clicks": 146,
#    "cost": 6165.352000003314,
#    "cpc": 42.228438356187084,
#    "ctr": 0.0009607981205209369,
#    "impressions": 151957
#  },
#  "today tried times": 1
#}

# testing_bidding_price_ortb_290__180.csv

# test_bidding_price_types_linear_75_base_bid.csv
#{
#  "best result": {
#    "clicks": 148,
#    "cost": 5988.161000002793,
#    "cpc": 40.46054729731617,
#    "ctr": 0.0010251295265009835,
#    "impressions": 144372
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 15,
#  "result": {
#    "clicks": 148,
#    "cost": 5988.161000002793,
#    "cpc": 40.46054729731617,
#    "ctr": 0.0010251295265009835,
#    "impressions": 144372
#  },
#  "today tried times": 1
#}

# test_bidding_price_types_linear_77_base_bid.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 15,
#  "result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "today tried times": 2
#}

# test_bidding_price_types_ortb_400__195.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 15,
#  "result": {
#    "clicks": 150,
#    "cost": 6197.769000002765,
#    "cpc": 41.31846000001843,
#    "ctr": 0.0009980836793356755,
#    "impressions": 150288
#  },
#  "today tried times": 3
#}

# test_bidding_price_types_ortb_390__205.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 15,
#  "result": {
#    "clicks": 146,
#    "cost": 6249.999000003089,
#    "cpc": 42.80821232878829,
#    "ctr": 0.000994299801821067,
#    "impressions": 146837
#  },
#  "today tried times": 4
#}

# test_bidding_price_types_linear_83_base_bid.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 15,
#  "result": {
#    "clicks": 144,
#    "cost": 6249.99900000277,
#    "cpc": 43.40277083335257,
#    "ctr": 0.001016812715807907,
#    "impressions": 141619
#  },
#  "today tried times": 5
#}

# test_bidding_price_types_ortb_334__80.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 17,
#  "result": {
#    "clicks": 149,
#    "cost": 6138.743000002659,
#    "cpc": 41.19961744968227,
#    "ctr": 0.000986872607330675,
#    "impressions": 150982
#  },
#  "today tried times": 1
#}

# test_bidding_price_types_ortb2_9.7e-07__17.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 18,
#  "result": {
#    "clicks": 136,
#    "cost": 6217.617000003079,
#    "cpc": 45.717772058846165,
#    "ctr": 0.0008470459273284421,
#    "impressions": 160558
#  },
#  "today tried times": 2
#}

# test_bidding_price_types_ortb1_3.9e-06__195.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 18,
#  "result": {
#    "clicks": 146,
#    "cost": 6249.999000003132,
#    "cpc": 42.80821232878858,
#    "ctr": 0.000991706346241365,
#    "impressions": 147221
#  },
#  "today tried times": 3
#}

# test_bidding_price_types_ortb1_3.98e-06__195.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 18,
#  "result": {
#    "clicks": 148,
#    "cost": 6249.999000003085,
#    "cpc": 42.22972297299382,
#    "ctr": 0.0009951854541542268,
#    "impressions": 148716
#  },
#  "today tried times": 4
#}

# test_bidding_price_types_linear_78_base_bid.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 18,
#  "result": {
#    "clicks": 147,
#    "cost": 6249.9990000031685,
#    "cpc": 42.517000000021554,
#    "ctr": 0.0010049083277505093,
#    "impressions": 146282
#  },
#  "today tried times": 5
#}

# test_bidding_price_types_semi_77_10.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 23,
#  "result": {
#    "clicks": 143,
#    "cost": 6249.999000002922,
#    "cpc": 43.70628671330714,
#    "ctr": 0.0009778780729647486,
#    "impressions": 146235
#  },
#  "today tried times": 1
#}

# testing_bidding_price_linear_regression.csv
#{
#  "best result": {
#    "clicks": 150,
#    "cost": 6088.526000002821,
#    "cpc": 40.59017333335214,
#    "ctr": 0.0010266799906914347,
#    "impressions": 146102
#  },
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 24,
#  "result": {
#    "clicks": 66,
#    "cost": 6249.999000000911,
#    "cpc": 94.69695454546834,
#    "ctr": 0.000823065795380855,
#    "impressions": 80188
#  },
#  "today tried times": 1
#}
