import itertools
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tabulate

from tqdm import tqdm
from mpl_toolkits.mplot3d import Axes3D  # noqa


data_path = Path("data")
train_path = data_path / "train.csv"
validation_path = data_path / "validation.csv"
test_path = data_path / "test.csv"

TRAIN_FOLDS = 8  # because validation is approx 8x size of train
BUDGET = 6_250_000
MAX_BID = 300
ITERATIONS = 5

RAND_BID_STEP = 10
RAND_BID_REPEATS = 5

BID_AXIS = range(0, MAX_BID + 1, RAND_BID_STEP)
ALL_BIDS = list(itertools.product(BID_AXIS, BID_AXIS))
ALL_BIDS = [(a, b) for a, b in ALL_BIDS if a < b]


class ConstantBidder:

    def __init__(self):
        self.train = self.load_dataset(train_path)
        self.train = self.train.sample(frac=1).reset_index(drop=True)
        self.train_fold = np.array_split(self.train, TRAIN_FOLDS)
        self.validation = self.load_dataset(validation_path)
        self.test = pd.read_csv(test_path, usecols=["bidid"], index_col="bidid")

    def load_dataset(self, path):
        return pd.read_csv(path, usecols=["bidid", "click", "payprice"], index_col="bidid")

    def reshuffle(self):
        self.train = self.train.sample(frac=1).reset_index(drop=True)
        self.train_fold = np.array_split(self.train, TRAIN_FOLDS)

    def get_won_subset(self, dataset, bid, budget):
        dataset = dataset[bid >= dataset["payprice"]]

        spend = dataset['payprice'].cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend.iloc[-1]
        return budget if max_spend > budget else max_spend, dataset

    def count_impressions_and_clicks(self, dataset, bid, budget):
        paid, subdataset = self.get_won_subset(dataset, bid, budget)

        impressions = len(subdataset)
        clicks = subdataset["click"].sum()

        return paid, impressions, clicks, subdataset

    @staticmethod
    def stats(paid, impressions, clicks):
        return {
            "impressions": impressions,
            "clicks": clicks,
            "CTR": clicks / impressions if impressions else None,
            "spent": paid / 1000,
            "CPM": paid / impressions if impressions else None,
            "CPC": (paid / 1000) / clicks if clicks else None,
        }

    def evaluate_bid_strategy(self, bid, budget, dataset):
        paid, impressions, clicks, subdataset = self.count_impressions_and_clicks(dataset, bid, budget)

        return self.stats(paid, impressions, clicks), subdataset

    def evaluate_train_bid_strategy(self, bid, budget, fold):
        return self.evaluate_bid_strategy(bid, budget, self.train_fold[fold])

    def evaluate_val_bid_strategy(self, bid, budget):
        return self.evaluate_bid_strategy(bid, budget, self.validation)


class RandomBidder(ConstantBidder):
    def get_won_subset(self, dataset, bid, budget):
        min_bid, max_bid = bid

        dataset = dataset[dataset["payprice"] <= max_bid]

        bids = np.random.randint(min_bid, max_bid, (len(dataset),))

        dataset = dataset[dataset["payprice"] <= bids]
        spend = dataset['payprice'].cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend.iloc[-1]
        return budget if max_spend > budget else max_spend, dataset


class RandomBidderWithOpponents(RandomBidder):
    def get_won_subset(self, dataset, bid, budget):
        min_bid, max_bid, extra_bidders = bid

        dataset = dataset[dataset["payprice"] <= max_bid]

        bids = np.random.randint(min_bid, max_bid, (len(dataset),))

        # makes N other bids, crates a new vector with the max of them and the payprice
        other_bids = np.random.randint(min_bid, max_bid, (len(dataset), extra_bidders + 1))
        other_bids[:, 0] = dataset["payprice"].values
        new_payprice = other_bids.max(axis=1)

        # reduces the dataset to those which were won and can be afforded
        dataset = dataset[new_payprice <= bids]
        payprices = new_payprice[new_payprice <= bids]
        spend = payprices.cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend[-1]
        return budget if max_spend > budget else max_spend, dataset


def prettify_table(table, columns):
    # adds vertical lines
    table = table.replace("{%s}" % ("r" * columns), "{|%s}" % ("r|" * columns))
    table = table.replace("{%s}" % ("l" * columns), "{|%s}" % ("r|" * columns))

    # add horizontal lines
    table = table.replace("\\\\\n", "\\\\ \\hline \n")

    # prettier header column line
    table = table.replace("\\hline \n\\hline", "\n\\hhline{|%s}" % ("=|" * columns), 1)

    # remove double bottom border
    table = table.replace("\\hline \n\\hline", "\\hline")

    return table


def run_constant_bidder():
    bidder = ConstantBidder()

    print(f"Train size: {bidder.train.shape[0]}")
    print(f"Train fold size: {bidder.train_fold[0].shape[0]}")
    print(f"Validation size: {bidder.validation.shape[0]}")
    print(f"Test size: {bidder.test.shape[0]}")

    clicks_train = np.zeros((MAX_BID, TRAIN_FOLDS))

    for _ in tqdm(range(ITERATIONS)):
        bidder.reshuffle()

        for bid in tqdm(range(1, MAX_BID)):
            for fold in range(TRAIN_FOLDS):
                result, train_data = bidder.evaluate_train_bid_strategy(bid, BUDGET, fold)
                clicks_train[bid][fold] += result["clicks"]

    clicks_train /= ITERATIONS

    mean_clicks_train = np.mean(clicks_train, axis=1)
    mean_clicks_train.tofile(f"results/constant_bidding_train.bin")

    fig, ax = plt.subplots(figsize=(8, 4.8))

    ax.set_xticks(range(0, MAX_BID + 10, 20))

    for i in range(TRAIN_FOLDS):
        ax.plot(clicks_train[:, i], label=f"Clicks in fold {i}", linewidth=0.75)

    ax.plot(mean_clicks_train, label="Mean clicks", linewidth=3)

    ax.set_ylabel("Clicks")
    ax.set_xlabel("Bidprice")
    ax.set_xlim(-1, 301)

    ax.legend()
    plt.tight_layout()

    plt.savefig("figures/constant_bidding_train.pdf", tight_layout=True)

    table = []

    for bid in (-np.array(mean_clicks_train)).argsort()[:10]:
        result, _ = bidder.evaluate_val_bid_strategy(bid, BUDGET)

        table.append([bid, mean_clicks_train[bid], result["clicks"]])

    table = tabulate.tabulate(table,
                              tablefmt='latex_raw',
                              headers=["Bid", "Train", "Validation"])

    Path("tables/constant_bidding_best.tex").write_text(prettify_table(table, 3))


def plot_2d_clicks(meaned, filename=None, label=None):
    fig = plt.figure(figsize=(8, 4.8))
    ax: Axes3D = fig.add_subplot(111, projection='3d')

    x_axis, y_axis = np.meshgrid(BID_AXIS, BID_AXIS)
    z_axis = np.zeros((len(BID_AXIS), len(BID_AXIS)))

    for i, x in enumerate(BID_AXIS):
        for j, y in enumerate(BID_AXIS):
            if (x, y) in ALL_BIDS:
                z_axis[i, j] = meaned[x, y]
            else:
                z_axis[i, j] = np.nan

    ax.set_xlabel("Min bid")
    ax.set_ylabel("Max bid")
    ax.set_zlabel("Clicks")

    ax.plot_wireframe(x_axis, y_axis, z_axis.transpose(), label=label)

    if label:
        ax.legend()

    fig.tight_layout()

    if filename:
        fig.savefig(filename)


def run_random_bidder():
    random_bidder = RandomBidder()

    clicks_train = np.zeros((MAX_BID, MAX_BID + 1, TRAIN_FOLDS))

    for _ in tqdm(range(RAND_BID_REPEATS)):
        random_bidder.reshuffle()

        for bid in tqdm(ALL_BIDS, leave=False):
            min_bid, max_bid = bid

            for fold in range(TRAIN_FOLDS):
                result, train_data = random_bidder.evaluate_train_bid_strategy(bid, BUDGET, fold)
                clicks_train[min_bid, max_bid, fold] += result["clicks"]

    clicks_train /= RAND_BID_REPEATS

    meaned = np.mean(clicks_train, axis=2)
    meaned.tofile(f"results/random_bidding_train.bin")

    plot_2d_clicks(meaned, "figures/random_bidding_train.pdf", label="Mean number of clicks")

    table = []

    for i in (-np.array(meaned.flat)).argsort()[:10]:
        x, y = np.unravel_index(i, meaned.shape)

        result, _ = random_bidder.evaluate_val_bid_strategy((x, y), BUDGET)

        table.append([f"{x}--{y}", meaned[x, y], result["clicks"]])

    table = tabulate.tabulate(table,
                              tablefmt='latex_raw',
                              headers=["Range", "Train", "Validation"])

    Path("tables/random_bidding_best.tex").write_text(prettify_table(table.replace("lrr", "rrr"), 3))


def random_bidder_with_opponents(bidder, num_of_other_bidders, repeats=1):
    clicks_train = np.zeros((MAX_BID, MAX_BID + 1, TRAIN_FOLDS))

    for _ in tqdm(range(repeats)):
        for bid in tqdm(ALL_BIDS, leave=False):
            min_bid, max_bid = bid

            for fold in range(TRAIN_FOLDS):
                result, train_data = bidder.evaluate_train_bid_strategy(
                    (min_bid, max_bid, num_of_other_bidders), BUDGET, fold
                )
                clicks_train[min_bid, max_bid, fold] += result["clicks"]

        clicks_train /= repeats

    meaned = np.mean(clicks_train, axis=2)
    meaned.tofile(f"results/random_bidding_train_against_{num_of_other_bidders}.bin")

    plot_2d_clicks(meaned, f"figures/random_bidding_train_against_{num_of_other_bidders}.pdf")

    table = []

    for i in (-np.array(meaned.flat)).argsort()[:10]:
        x, y = np.unravel_index(i, meaned.shape)

        result, _ = bidder.evaluate_val_bid_strategy((x, y, num_of_other_bidders), BUDGET)

        table.append([x, y, meaned[x, y], result["clicks"]])

    table = tabulate.tabulate(table,
                              tablefmt='latex_raw',
                              headers=["Lower bound", "Upper bound", "Train", "Validation"])

    Path(f"tables/random_bidding_best_{num_of_other_bidders}.tex").write_text(prettify_table(table, 4))


if __name__ == "__main__":

    if sys.argv[1] == "simple":
        run_constant_bidder()
        run_random_bidder()
    else:
        bidder = RandomBidderWithOpponents()

        if sys.argv[1] == "1":
            random_bidder_with_opponents(bidder, 10, 2)

        if sys.argv[1] == "2":
            random_bidder_with_opponents(bidder, 50, 2)
            random_bidder_with_opponents(bidder, 60, 2)

        if sys.argv[1] == "3":
            random_bidder_with_opponents(bidder, 70, 2)
            random_bidder_with_opponents(bidder, 80, 2)

        if sys.argv[1] == "4":
            random_bidder_with_opponents(bidder, 90, 2)

        if sys.argv[1] == "5":
            random_bidder_with_opponents(bidder, 100, 2)
