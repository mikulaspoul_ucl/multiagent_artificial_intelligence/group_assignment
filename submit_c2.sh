#!/bin/bash

TOKEN=`cat secret_token`

curl http://deepmining.cs.ucl.ac.uk/api/upload/wining_criteria_2/$TOKEN -X Post -F 'file=@results/test_bidding_price_types_ortb2_reduced2_4.7e-07__128.csv'

# attempt ???

#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 5,
#  "result": {
#    "clicks": 16,
#    "cost": 6250.022849396316,
#    "cpc": 390.62642808726974,
#    "ctr": 0.0006534346156987666,
#    "impressions": 24486
#  },
#  "today tried times": 3
#}


# testing_bidding_price_semi_linear_20.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 3,
#  "result": {
#    "clicks": 29,
#    "cost": 6250.17900101405,
#    "cpc": 215.5234138280707,
#    "ctr": 0.0019609168976942324,
#    "impressions": 14789
#  },
#  "today tried times": 2
#}

# testing_bidding_price_semi_linear_10.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 3,
#  "result": {
#    "clicks": 29,
#    "cost": 6250.0112158681995,
#    "cpc": 215.51762813338618,
#    "ctr": 0.0019599891862665587,
#    "impressions": 14796
#  },
#  "today tried times": 3
#}

# testing_bidding_price_linear.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 3,
#  "result": {
#    "clicks": 29,
#    "cost": 6250.128190735868,
#    "cpc": 215.5216617495127,
#    "ctr": 0.001959327072495102,
#    "impressions": 14801
#  },
#  "today tried times": 4
#}

# testing_bidding_price_linear_110_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 2,
#  "result": {
#    "clicks": 51,
#    "cost": 4453.599957071292,
#    "cpc": 87.32548935433906,
#    "ctr": 0.005065051147085112,
#    "impressions": 10069
#  },
#  "today tried times": 1
#}

# testing_bidding_price_linear_130_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 2,
#  "result": {
#    "clicks": 54,
#    "cost": 5063.449106056043,
#    "cpc": 93.76757603807488,
#    "ctr": 0.00457433290978399,
#    "impressions": 11805
#  },
#  "today tried times": 2
#}

# testing_bidding_price_ortb_305__180.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 9,
#  "result": {
#    "clicks": 6,
#    "cost": 1542.0734430354842,
#    "cpc": 257.012240505914,
#    "ctr": 0.0008301051466519093,
#    "impressions": 7228
#  },
#  "today tried times": 1
#}

# testing_bidding_price_linear_140_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 6,
#  "result": {
#    "clicks": 17,
#    "cost": 5661.5847168206965,
#    "cpc": 333.0343951070998,
#    "ctr": 0.0009801095416546554,
#    "impressions": 17345
#  },
#  "today tried times": 2
#}

# testing_bidding_price_pctr2_linear_130_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 6,
#  "result": {
#    "clicks": 16,
#    "cost": 5116.702299016837,
#    "cpc": 319.7938936885523,
#    "ctr": 0.0011107254425546686,
#    "impressions": 14405
#  },
#  "today tried times": 3
#}

# testing_bidding_price_pctr2_linear_150_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 7,
#  "result": {
#    "clicks": 16,
#    "cost": 6250.512217323765,
#    "cpc": 390.6570135827353,
#    "ctr": 0.0007269092726364091,
#    "impressions": 22011
#  },
#  "today tried times": 4
#}

# testing_bidding_price_linear_150_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 4,
#  "result": {
#    "clicks": 19,
#    "cost": 6251.906650607758,
#    "cpc": 329.04771845303986,
#    "ctr": 0.0008572846636285701,
#    "impressions": 22163
#  },
#  "today tried times": 5
#}

# test_bidding_random_5000_10000.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 5,
#  "result": {
#    "clicks": 14,
#    "cost": 6249.998428954699,
#    "cpc": 446.4284592110499,
#    "ctr": 0.0006459948320413437,
#    "impressions": 21672
#  },
#  "today tried times": 1
#}

# test_bidding_random_2500_7500.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 5,
#  "result": {
#    "clicks": 14,
#    "cost": 6249.979490114673,
#    "cpc": 446.42710643676236,
#    "ctr": 0.0006023059714334883,
#    "impressions": 23244
#  },
#  "today tried times": 2
#}

# test_bidding_price_types_linear_83_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 9,
#  "result": {
#    "clicks": 7,
#    "cost": 2476.6057401938306,
#    "cpc": 353.80082002769007,
#    "ctr": 0.0010583610523132749,
#    "impressions": 6614
#  },
#  "today tried times": 3
#}

# test_bidding_price_types_linear_120_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 9,
#  "result": {
#    "clicks": 9,
#    "cost": 4195.510646547854,
#    "cpc": 466.1678496164282,
#    "ctr": 0.0006756756756756757,
#    "impressions": 13320
#  },
#  "today tried times": 4
#}

# test_bidding_price_types_ortb_400__260.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 14,
#  "result": {
#    "clicks": 2,
#    "cost": 1089.7108961575589,
#    "cpc": 544.8554480787794,
#    "ctr": 0.0004017677782241864,
#    "impressions": 4978
#  },
#  "today tried times": 5
#}

# test_bidding_price_types_linear_301_base_bid.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 9,
#  "result": {
#    "clicks": 8,
#    "cost": 6249.98725709028,
#    "cpc": 781.248407136285,
#    "ctr": 0.0002101392172314158,
#    "impressions": 38070
#  },
#  "today tried times": 1
#}

# test_bidding_price_types_ortb2_9.7e-07__64.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 10,
#  "result": {
#    "clicks": 6,
#    "cost": 6249.996989225537,
#    "cpc": 1041.6661648709228,
#    "ctr": 9.779951100244499e-05,
#    "impressions": 61350
#  },
#  "today tried times": 2
#}

# test_bidding_price_types_ortb2_reduced_7.7e-07__96.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 13,
#  "result": {
#    "clicks": 4,
#    "cost": 1829.3892149628084,
#    "cpc": 457.3473037407021,
#    "ctr": 0.0005647324579980235,
#    "impressions": 7083
#  },
#  "today tried times": 3
#}

# test_bidding_price_types_ortb2_reduced2_7.7e-07__128.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 6,
#  "result": {
#    "clicks": 13,
#    "cost": 6249.972525607696,
#    "cpc": 480.7671173544382,
#    "ctr": 0.00034998923110058153,
#    "impressions": 37144
#  },
#  "today tried times": 4
#}

# test_bidding_price_types_ortb2_reduced2_5.7e-07__128.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 5,
#  "result": {
#    "clicks": 15,
#    "cost": 6249.986379933438,
#    "cpc": 416.6657586622292,
#    "ctr": 0.00038407374215849446,
#    "impressions": 39055
#  },
#  "today tried times": 5
#}

# test_bidding_price_types_ortb2_reduced2_4.7e-07__128.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 6,
#  "result": {
#    "clicks": 13,
#    "cost": 6249.9848376019945,
#    "cpc": 480.76806443092266,
#    "ctr": 0.00036454389949805107,
#    "impressions": 35661
#  },
#  "today tried times": 1
#}

#  test_bidding_price_types_ortb1_reduced2_3e-06__290.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 14,
#  "result": {
#    "clicks": 5,
#    "cost": 1948.121475487511,
#    "cpc": 389.62429509750217,
#    "ctr": 0.0008257638315441783,
#    "impressions": 6055
#  },
#  "today tried times": 1
#}

# test_bidding_price_types_ortb2_reduced2_4.7e-07__128.csv
#{
#  "daily submission limit": 5,
#  "group": "2",
#  "ranking": 9,
#  "result": {
#    "clicks": 11,
#    "cost": 6249.994533365283,
#    "cpc": 568.1813212150257,
#    "ctr": 0.00034010450483875954,
#    "impressions": 32343
#  },
#  "today tried times": 2
#}
