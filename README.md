# MAAI Group Assignment

Group 2 --  Mikuláš Poul, Charles Shelbourne, Shahrooz Aliabadi-Zadeh

The training/validation/test data should be downloaded in the data folder.

A large portion of the scripts is in a state of experimentation with some stuff commented out which are actually required to make stuff run properly.

List of scripts and notebooks:

  - `basic_bidders.py` -- constant and random bidders, bidding against opponents
  - `opponents_plot.py` -- creates a 3D plot of bidding against opponents
  - `opponents_table.py` -- creates a table with results of bidding against opponents
  - `usertags_and_area.ipynb` -- feature engineering, creates extended files
  - `Linear_bidder_logistic.ipynb` -- linear bidding with logistic regression
  - `linear_bidder_gbm.py` -- linear bidding with gradient boosting trees
  - `semi_random_bidder.py` -- semi-random bidding based on `linear_bidder_gbm.py`
  - `ortb_bidder.py` -- ORTB1 and ORTB2 bidders
  - `multiagent_bidder.py` -- multi-agent bidding strategy
  - `multiagent_table.py` -- creates a table with comparison of strategies
  - `payprice_stats.ipynb` -- Some basic statistics of payprice
  - `submit_c1.sh` -- submitting to #1 and results
  - `submit_c2.sh` -- submitting to #2 and results
  - `Pipfile` -- dependencies
  - `Pipfile.lock` -- dependencies lock

Folders:
  - `data` -- for data files
  - `figures` -- plots
  - `models` -- for saved models
  - `results` -- various saved matricies and submission files
  - `tables` -- tables, generated and manually created
