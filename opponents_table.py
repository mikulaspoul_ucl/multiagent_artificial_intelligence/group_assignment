from basic_bidders import *

bidder = RandomBidderWithOpponents()

table = []

for n in 50, 60, 70, 80, 90, 100:

    meaned = np.fromfile(f"results/random_bidding_train_against_{n}.bin").reshape((300, 301))

    for i in (-np.array(meaned.flat)).argsort()[:3]:
        x, y = np.unravel_index(i, meaned.shape)

        result, _ = bidder.evaluate_val_bid_strategy((x, y, n), BUDGET)

        table.append([n, f"{x}--{y}", meaned[x, y], result["clicks"]])

table = tabulate.tabulate(table,
                          tablefmt='latex_raw',
                          headers=["Opponents", "Range", "Train", "Validation"])

table = prettify_table(table.replace("rlrr", "rrrr"), 4)


def replace_on_line(x: str, line: int):
    lines = x.splitlines()

    lines[line] = lines[line].replace("\\hline", "\\hhline{|=|=|=|=|}")

    return "\n".join(lines)


table = replace_on_line(table, 6)
table = replace_on_line(table, 9)
table = replace_on_line(table, 12)
table = replace_on_line(table, 15)
table = replace_on_line(table, 18)


Path(f"tables/random_bidding_opponents_best.tex").write_text(table)
