from basic_bidders import *


fig = plt.figure(figsize=(8, 4.8))
ax: Axes3D = fig.add_subplot(111, projection='3d')


# seaborn.color_palette("colorblind", 6)
colors = [(0.00392156862745098, 0.45098039215686275, 0.6980392156862745),
          (0.8705882352941177, 0.5607843137254902, 0.0196078431372549),
          (0.00784313725490196, 0.6196078431372549, 0.45098039215686275),
          (0.8352941176470589, 0.3686274509803922, 0.0),
          (0.8, 0.47058823529411764, 0.7372549019607844),
          (0.792156862745098, 0.5686274509803921, 0.3803921568627451)]

for n in 50, 60, 70, 80, 90, 100:

    meaned = np.fromfile(f"results/random_bidding_train_against_{n}.bin").reshape((300, 301))

    x_axis, y_axis = np.meshgrid(BID_AXIS, BID_AXIS)
    z_axis = np.zeros((len(BID_AXIS), len(BID_AXIS)))

    for i, x in enumerate(BID_AXIS):
        for j, y in enumerate(BID_AXIS):
            if (x, y) in ALL_BIDS:
                z_axis[i, j] = meaned[x, y]
            else:
                z_axis[i, j] = np.nan

    ax.set_xlabel("Min bid")
    ax.set_ylabel("Max bid")
    ax.set_zlabel("Clicks")

    x = ax.plot_wireframe(x_axis, y_axis, z_axis.transpose(),
                          label=f"Against {n} opponents",
                          linewidth=0.3)

    x.set_edgecolor(colors[(n // 10) - 5])

ax.legend()

ax.view_init(elev=20, azim=30)

fig.tight_layout()
fig.savefig("figures/random_bidding_against_opponents.pdf")
