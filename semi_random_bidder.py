import pandas as pd

import lightgbm
from matplotlib import pyplot as plt
from tqdm import tqdm

import numpy as np

from linear_bidder_gbm import LinearBidder, BUDGET, test_path, validation_path


class SemiRandomBidder(LinearBidder):

    def __init__(self, load_validation=True):
        super().__init__(load_train=False, load_validation=load_validation)

    def get_bids(self, pctr, base_bid, std):
        bids = base_bid * (pctr / self.average_ctr)

        bids = bids + np.random.normal(scale=std, size=bids.shape)

        return bids

    def get_won_subset(self, x, y, bid, budget):
        pctr, base_bid, std = bid

        bids = self.get_bids(pctr, base_bid, std)

        dataset = y[bids >= y["payprice"]]

        spend = dataset['payprice'].cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend.iloc[-1]
        return budget if max_spend > budget else max_spend, dataset

    def bid_on_test(self, bid):
        model, base_bid, std = bid

        test_x, test_ids = self.load_dataset(test_path, test=True)

        pctr = model.predict(test_x)

        print("Loaded test")

        bids = self.get_bids(pctr, base_bid, std)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df

    def bid_on_validation(self, bid):
        model, base_bid, std = bid

        test_x, test_ids = self.load_dataset(validation_path, test=True)

        pctr = model.predict(test_x)

        print("Loaded validation")

        bids = self.get_bids(pctr, base_bid, std)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df


def load_model(x=None):
    return lightgbm.Booster(model_file=x or "models/proper_types_full.model")


def run_bidder(bidder: SemiRandomBidder):
    model = load_model()

    min_bid = 30
    max_bid = 110
    skip = 5

    bid_combinations = list(range(min_bid, max_bid, skip))

    pctr = model.predict(bidder.X_validation)

    fig, ax = plt.subplots(figsize=(8, 4.8))

    for std in tqdm(range(0, 70, 10)):

        table = []

        for i, base_bid in tqdm(enumerate(bid_combinations), total=len(bid_combinations)):
            clicks = []

            for _ in range(10):
                result, _ = bidder.evaluate_val_bid_strategy((pctr, base_bid, std), BUDGET)
                clicks.append(result["clicks"])

            table.append([base_bid, np.mean(clicks)])

        ax.plot([x[0] for x in table], [x[1] for x in table], label=f"Clicks with random addition of std {std}")

    ax.set_xticks(range(min_bid, max_bid + 1, skip * 2))
    ax.set_ylabel("Clicks")
    ax.set_xlabel("Base bid")
    ax.set_xlim(min_bid - 1, max_bid + 1)
    ax.legend(loc=8)
    plt.tight_layout()
    plt.savefig(f"figures/semi_random_bidder.pdf", tight_layout=True)

    print(" ")


if __name__ == "__main__":
    bidder = SemiRandomBidder(load_validation=False)

    # run_bidder(bidder)

    df = bidder.bid_on_validation((load_model(), 77, 30))
    #
    df.to_csv("results/val_bid_price_semi_linear_77_30.csv", index=None)
