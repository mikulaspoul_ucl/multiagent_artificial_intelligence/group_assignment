import gc
from pathlib import Path

import lightgbm
import numpy as np
import pandas as pd
import sklearn
import tabulate
from matplotlib import pyplot as plt
from sklearn.model_selection import StratifiedKFold
from tqdm import tqdm

from basic_bidders import prettify_table

MAX_BID = 300
TRAIN_FOLDS = 8
FOLD_SIZE = 303_873
BUDGET = 6_250_000
AVERAGE_CTR = 0.0007375623256619447

data_path = Path("data")
train_path = data_path / "train_extended.csv"
validation_path = data_path / "validation_extended.csv"
test_path = data_path / "test_extended.csv"


class LinearBidder:

    def __init__(self, load_train=True, load_validation=True):
        if load_train:
            self.X_train, self.y_train = self.load_dataset(train_path)
            print("Loaded train")

            sklearn.utils.shuffle(self.X_train, self.y_train)

        if load_validation:
            self.X_validation, self.y_validation = self.load_dataset(validation_path)
            print("Loaded validation")

            sklearn.utils.shuffle(self.X_validation, self.y_validation)

        self.average_ctr = AVERAGE_CTR

    def load_dataset(self, path, test=False):
        usecols = {"slotwidth", "slotheight", "slotarea", "weekday", "hour", "useragent", "region", "city",
                   "adexchange", "domain", "slotid", "slotvisibility", "slotformat", "slotprice",
                   "creative", "keypage", "advertiser", "os", "browser"}

        categories = {"useragent", "IP", "region", "city", "adexchange", "domain", "url",
                      "urlid", "slotid", "slotvisibility", "creative", "keypage", "advertiser", "slotformat",
                      "os", "browser"}

        one_row = pd.read_csv(path, nrows=1)

        usecols = [x for x in one_row.columns if x in usecols or "usertag_" in x]

        categories = {k: "category" for k in usecols if (k in categories or "usertag_" in k)}

        dataset = pd.read_csv(path, usecols=usecols, nrows=TRAIN_FOLDS * FOLD_SIZE, dtype=categories)

        if test:
            ids = pd.read_csv(path, usecols=["bidid"])

            return dataset, ids
        else:
            y = pd.read_csv(path, usecols=["click", "payprice", "bidprice"],
                            nrows=TRAIN_FOLDS * FOLD_SIZE)

            return dataset, y

    def get_bids(self, pctr, base_bid):
        return base_bid * (pctr / self.average_ctr)

    def get_won_subset(self, x, y, bid, budget):
        pctr, base_bid = bid

        bids = self.get_bids(pctr, base_bid)

        dataset = y[bids >= y["payprice"]]

        spend = dataset['payprice'].cumsum()
        dataset = dataset[spend <= budget]

        max_spend = spend.iloc[-1]
        return budget if max_spend > budget else max_spend, dataset

    def count_impressions_and_clicks(self, x, y, bid, budget):
        paid, subdataset = self.get_won_subset(x, y, bid, budget)

        impressions = len(subdataset)
        clicks = subdataset["click"].sum()

        return paid, impressions, clicks, subdataset

    @staticmethod
    def stats(paid, impressions, clicks):
        return {
            "impressions": impressions,
            "clicks": clicks,
            "CTR": clicks / impressions if impressions else None,
            "spent": paid / 1000,
            "CPM": paid / impressions if impressions else None,
            "CPC": (paid / 1000) / clicks if clicks else None,
        }

    def evaluate_bid_strategy(self, bid, budget, x, y):
        paid, impressions, clicks, subdataset = self.count_impressions_and_clicks(x, y, bid, budget)

        return self.stats(paid, impressions, clicks), subdataset

    def evaluate_train_bid_strategy(self, bid, budget, X_train, y_train):
        return self.evaluate_bid_strategy(bid, budget, X_train, y_train)

    def evaluate_val_bid_strategy(self, bid, budget):
        return self.evaluate_bid_strategy(bid, budget, self.X_validation, self.y_validation)

    def bid_on_test(self, bid):
        model, base_bid = bid

        test_x, test_ids = self.load_dataset(test_path, test=True)

        print("Loaded test")

        pctr = model.predict(test_x)

        bids = self.get_bids(pctr, base_bid)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df

    def bid_on_validation(self, bid):
        model, base_bid = bid

        test_x, test_ids = self.load_dataset(validation_path, test=True)

        pctr = model.predict(test_x)

        print("Loaded validation")

        bids = self.get_bids(pctr, base_bid)
        bids[bids < 0] = 0

        bids = bids.astype(np.float)

        df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

        return df


def train_model(X_train, y_train, X_validation=None, y_validation=None, iterations=None, weight=0.42, update=None):
    train = lightgbm.Dataset(X_train, label=y_train["click"].values,
                             weight=(1 - weight) + y_train["click"].values * weight)
    valid_sets = [train]

    if X_validation is not None:
        valid = lightgbm.Dataset(X_validation, label=y_validation["click"].values,
                                 weight=1 + y_validation["click"].values * weight)
        valid_sets.append(valid)

    params = {"metric": "rmse", "verbosity": 1,
              "bagging_fraction": 0.85, "feature_fraction": 0.85,
              "lambda_l2": 0.04, "learning_rate": 0.2,
              "num_leaves": 31, "boosting": "dart", "n_jobs": 3}

    if update:
        params.update(update)

    return lightgbm.train(params,
                          train_set=train, valid_sets=valid_sets,
                          num_boost_round=iterations or 45)


def run_prediction_bidder(bidder):
    print(f"Train size: {bidder.X_train.shape[0]}")
    print(f"Validation size: {bidder.X_validation.shape[0]}")

    clicks_train = np.zeros((MAX_BID, TRAIN_FOLDS))

    kfold = StratifiedKFold(n_splits=TRAIN_FOLDS)

    for i, (train_index, test_index) in enumerate(kfold.split(bidder.X_train, bidder.y_train["click"].values)):
        X_train, X_test = bidder.X_train.iloc[train_index], bidder.X_train.iloc[test_index]
        y_train, y_test = bidder.y_train.iloc[train_index], bidder.y_train.iloc[test_index]

        model = train_model(X_train, y_train, X_test, y_test)

        pctr = model.predict(X_test)

        for bid in tqdm(range(1, MAX_BID), desc=f"Fold {i}"):
            result, train_data = bidder.evaluate_train_bid_strategy((pctr, bid), BUDGET, X_test, y_test)
            clicks_train[bid][i] = result["clicks"]

    print("")

    np.savez("results/gbm_linear_bidder_train.npz", clicks_train)

    mean_clicks_train = np.mean(clicks_train, axis=1)

    fig, ax = plt.subplots(figsize=(8, 4.8))

    ax.set_xticks(range(0, MAX_BID + 10, 20))

    for i in range(clicks_train.shape[1]):
        ax.plot([x for x in range(1, (MAX_BID) + 1)],
                clicks_train[:, i], label=f"Clicks in fold {i}", linewidth=0.75)

    ax.plot([x for x in range(1, (MAX_BID) + 1)],
            mean_clicks_train, label="Mean clicks", linewidth=3)

    ax.set_ylabel("Clicks")
    ax.set_xlabel("Base bid")
    ax.set_xlim(-1, MAX_BID + 1)

    ax.legend()
    plt.tight_layout()

    plt.savefig(f"figures/linear_bidding_train.pdf", tight_layout=True)

    model = lightgbm.Booster(model_file="models/proper_types_full.model")

    # model = train_model(bidder.X_train, bidder.y_train)
    # model.save_model(f"models/proper_types_full.model")

    table = []

    pctr = model.predict(bidder.X_validation)

    for bid in tqdm((-np.array(mean_clicks_train)).argsort()[:10], desc="Validation performance"):
        result, _ = bidder.evaluate_val_bid_strategy((pctr, bid), BUDGET)

        table.append([bid, mean_clicks_train[bid], result["clicks"]])

    print("")

    table = tabulate.tabulate(table,
                              tablefmt='latex_raw',
                              headers=["Base bid", "Train", "Validation"])

    Path(f"tables/linear_bidding_best.tex").write_text(prettify_table(table, 3))


def get_precise_number(bidder):
    min_bid = 60
    max_bid = 101

    table = []

    full_model = lightgbm.Booster(model_file="models/proper_types_full.model")

    pctr = full_model.predict(bidder.X_validation)

    for base_bid in tqdm(range(min_bid, max_bid)):
        x = [base_bid]

        result, _ = bidder.evaluate_val_bid_strategy((pctr, base_bid), BUDGET)

        x.append(result["clicks"])

        table.append(x)

    fig, ax = plt.subplots(figsize=(8, 4.8))

    ax.set_xticks(range(min_bid, max_bid + 1, 5))

    ax.plot([x[0] for x in table], [x[1] for x in table], label="Clicks on validation set")

    ax.set_ylabel("Clicks")
    ax.set_xlabel("Base bid")
    ax.set_xlim(min_bid - 1, max_bid + 1)

    ax.legend()
    plt.tight_layout()
    plt.savefig(f"figures/linear_bidding_precise_base_bid.pdf", tight_layout=True)

    table = tabulate.tabulate(table,
                              tablefmt='latex_raw',
                              headers=["Base bid", "Full"])
    Path(f"tables/linear_bidding_precise_base_bid.tex").write_text(prettify_table(table, 2))


if __name__ == "__main__":
    bidder = LinearBidder(load_train=False, load_validation=False)
    gc.collect()

    # run_prediction_bidder(bidder)
    # get_precise_number(bidder)
    #
    model = lightgbm.Booster(model_file="models/proper_types_full.model")

    df = bidder.bid_on_validation((model, 77))
    #
    df.to_csv("results/val_bid_price_linear_77.csv", index=None)
