import json
from pathlib import Path

import tabulate

from basic_bidders import prettify_table

results = json.loads(Path("results/ma_comparison.json").read_text())


table = []

for n, d in [
    ("bid", "Multi-Agent"),
    ("linear_77", "Linear (77)"),
    ("semi_linear_77_30", "Semi-linear (77,  $\\sigma=30$)"),
    ("ortb1_400_195", "ORTB1 ($\lambda = 4\mathrm{e}{-6}$, $c = 195$)"),
    ("ortb2_8.5e-07_32", "ORRB2 ($\lambda = 8.5\mathrm{e}{-7}$, $c = 32$)"),
    ("constant_77", "Constant (77)"),
    ("random_50_90", "Random (50--90)"),
]:

    r = results.get(n, {})

    table.append(
        (d, r.get("impressions") or 0, r.get("clicks") or 0, round(r.get("spent") or 0, 2))
    )

table = tabulate.tabulate(table,
                          tablefmt='latex_raw',
                          headers=["Bidder", "Impressions", "Clicks", "Spent"])

table = prettify_table(table.replace("lrrr", "rrrr"), 4).replace("|r|r|r|r|", "|l|r|r|r|")


Path(f"tables/multi_agent_comparison.tex").write_text(table)
