import numpy as np
import pandas as pd

from linear_bidder_gbm import LinearBidder, test_path, validation_path


def make_random_bids(min_bid, max_bid):
    test_x, test_ids = LinearBidder(load_validation=False, load_train=False).load_dataset(validation_path, test=True)

    print("Loaded test")

    bids = np.random.uniform(float(min_bid), float(max_bid), (len(test_x),))

    df = pd.DataFrame({"bidid": test_ids["bidid"].values, "bidprice": bids}, index=None)

    return df


if __name__ == "__main__":

    min_bid = 5000
    max_bid = 10000

    df = make_random_bids(min_bid, max_bid)

    df.to_csv(f"results/val_bid_price_random_50_100.csv", index=None)
